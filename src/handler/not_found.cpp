#include "handler.hpp"
#include "../copper.hpp"

using namespace Copper;

Response NotFoundHandler::handle(HTTPServer&, Call&) const
{
	static Response response(NOT_FOUND);
	response.set_body("<html><head><title>404 - Not found</title></head><body><h1>404 - Not found</h1><p>The requested page doesn't exist!</p></body></html>");

	return response;
}
