#include "../copper.hpp"

#define BUFF_SIZE	8192

using namespace Copper;

// TODO Shrink buffer to not read data
void HTTPStream::read(const int sock)
{
	if(!sock_is_ready(sock)) return;

	char* buff = (char*) malloc(BUFF_SIZE);
	if(!buff) throw bad_alloc();

	const auto length = sock_read(sock, buff, BUFF_SIZE);

	if(length > 0) {
		try {
			buffer_append(buff, static_cast<size_t>(length));
		} catch(const bad_alloc& e) {
			free((void*) buff);
			throw e;
		}
	}

	free((void*) buff);
}

void HTTPStream::buffer_append(const char* buff, const size_t length)
{
	if(buffer) {
		const auto diff = i - buffer;

		buffer = (char*) realloc((void*) buffer, this->length + length);
		i = buffer + diff;
	} else {
		buffer = (char*) malloc(length);
		i = buffer;
	}

	if(!buffer) throw bad_alloc();

	memcpy(buffer + this->length, buff, length);
	this->length += length;
}

bool HTTPStream::check_line() const
{
	for(auto j = i; j < (buffer + length); ++j) {
		if(j[0] == '\r' && j + 1 < (buffer + length) && j[1] == '\n') {
			return true;
		}
	}

	return false;
}

bool HTTPStream::get_line(string& buff)
{
	auto j = i;

	while(j < (buffer + length)) {
		if(j[0] == '\r' && j + 1 < (buffer + length) && j[1] == '\n') {
			buff.resize(j - i);
			memcpy(&buff[0], i, j - i);
			i = j + 2;

			return true;
		}

		++j;
	}

	return false;
}

bool HTTPStream::get_line(string& buff, const char c)
{
	auto j = i;

	while(j < (buffer + length)) {
		if(*j == c) {
			buff.resize(j - i);
			memcpy(&buff[0], i, j - i);
			i = j + 1;

			return true;
		}

		++j;
	}

	return false;
}

string HTTPStream::get()
{
	const auto size = buffer + length - i;

	string str;
	str.resize(size);
	memcpy(&str[0], i, size);

	i += size;

	return str;
}
