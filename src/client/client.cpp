#include "../copper.hpp"

using namespace Copper;

Client::Client(HTTPServer& server, const sock_t& sock, const bool https)
	: server{&server}, sock{sock}, https{https}, call(new Call(server, sock, https))
{
	server.log("New connection from " + sock.host);
}

Client::~Client()
{
	server->log("Connection from " + sock.host + " closed");
}

bool Client::handle()
{
	try {
		if(call->handle()) {
			server->handle_call(*call, call->get_default_code());
			call = shared_ptr<Call>(new Call(*server, sock, https));
		}

		return true;
	} catch(const socket_exception&) {
		return false;
	} catch(const bad_alloc& e) {
		throw e;
	} catch(const exception&) {
		server->handle_call(*call, INTERNAL_SERVER_ERROR);
		call = shared_ptr<Call>(new Call(*server, sock, https));
		return false;
	}
}
