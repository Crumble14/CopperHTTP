#include "../copper.hpp"

#define MAX_HEAD_LENGTH	1048576

using namespace Copper;

const unordered_map<string, http_method> methods = {
	{"GET", GET},
	{"HEAD", HEAD},
	{"POST", POST},
	{"PUT", PUT},
	{"DELETE", DELETE},
	{"TRACE", TRACE},
	{"OPTIONS", OPTIONS},
	{"CONNECT", CONNECT},
	{"PATCH", PATCH}
};

static char to_hexa(const char c)
{
	if(c >= '0' && c <= '9') {
		return (c - '0');
	} else if(c >= 'a' && c <= 'f') {
		return 10 + (c - 'a');
	} else if(c >= 'A' && c <= 'F') {
		return 10 + (c - 'A');
	} else {
		throw invalid_argument("Invalid character!");
	}
}

static void translate_hexadecimal(string& str)
{
	for(unsigned int i = 0; i < str.size(); ++i) {
		if(str[i] != '%') continue;
		if(i >= str.size() - 3) throw invalid_argument("Invalid hexadecmial value!");

		str[i] = (to_hexa(str[i + 1]) * 16) + to_hexa(str[i + 2]);

		if(str[i] == '\0') {
			throw invalid_argument("Invalid character!");
		}

		str.erase(i + 1, 2);
	}
}

static inline http_method get_http_method(const string& buffer)
{
	const auto m = methods.find(buffer);
	return (m != methods.cend() ? m->second : UNKNOWN);
}

static inline bool check_version(const string&)
{
	// TODO
	return true;
}

static inline string low_str(const string& str)
{
	string s;
	s.reserve(str.size());

	for(const auto& c : str) s += tolower(c);

	return s;
}

bool Call::handle()
{
	stream.read(sock.fd);

	if(stream.data_length() > MAX_HEAD_LENGTH) {
		default_code = REQUEST_HEADER_FIELDS_TOO_LARGE;
		return true;
	}

	switch(parse_state) {
		case HEADER_S: {
			default_code = parse_header();
			break;
		}

		case HEAD_S: {
			default_code = parse_head();
			break;
		}

		case BODY_S: {
			default_code = read_body();
			break;
		}

		case DONE_S: {
			return true;
		}
	}

	return (default_code >= 400);
}

http_code_t Call::parse_header()
{
	if(!stream.check_line()) return OK;

	string buff;
	if(!stream.get_line(buff, ' ')) return BAD_REQUEST;
	method = get_http_method(buff);
	if(method == UNKNOWN) return BAD_REQUEST;

	if(!stream.get_line(full_path, ' ')) return BAD_REQUEST;
	translate_hexadecimal(full_path);
	if(!parse_path()) return BAD_REQUEST;

	if(!stream.get_line(http_version)) return BAD_REQUEST;
	if(!check_version(http_version)) return BAD_REQUEST;

	parse_state = HEAD_S;
	return OK;
}

bool Call::parse_path()
{
	if(full_path.empty()) return false;

	size_t pos;

	if((pos = full_path.find('?')) == string::npos) {
		path = full_path;
		return true;
	}

	path = string(full_path.cbegin(), full_path.cbegin() + pos);

	stringstream ss(path);
	string buffer;

	while(getline(ss, buffer, '/')) {
		if(buffer.empty()) continue;
		split_path.push_back(buffer);
	}

	const string str(full_path.cbegin() + pos + 1, full_path.cend());
	if(str.empty()) return true;

	ss.str(str);

	while(getline(ss, buffer, '&')) {
		size_t p;

		if((p = buffer.find('=')) != string::npos) {
			const string name(buffer.cbegin(), buffer.cbegin() + pos);
			string value(buffer.cbegin() + pos + 1, buffer.cend());

			head_fields.emplace(name, value);
		} else {
			head_fields.emplace(buffer, string());
		}
	}

	return true;
}

http_code_t Call::parse_head()
{
	string buffer;

	while(stream.get_line(buffer)) {
		if(buffer.empty()) {
			parse_state = BODY_S;
			break;
		}

		size_t pos;

		if((pos = buffer.find(':')) != string::npos) {
			const string name(buffer.cbegin(), buffer.cbegin() + pos);
			string value(buffer.cbegin() + pos + 1, buffer.cend());

			while(!value.empty() && value.front() == ' ') value.erase(0, 1);

			head_fields.emplace(name, value);
		} else {
			head_fields.emplace(buffer, string());
		}
	}

	return OK;
}

http_code_t Call::read_body()
{
	// TODO

	parse_state = DONE_S;
	return OK;
}

const string& Call::get_arg(const string& key) const
{
	const auto a = args.find(key);
	if(a == args.cend()) throw invalid_argument("Key not found!");

	return a->second;
}

bool Call::has_field(const string& key) const
{
	const string str(low_str(key));

	for(const auto& f : head_fields) {
		if(low_str(f.first) == str) return true;
	}

	return false;
}

const string& Call::get_field(const string& key) const
{
	const string str(low_str(key));

	for(const auto& f : head_fields) {
		if(low_str(f.first) == str) return f.second;
	}

	throw invalid_argument("Field not found!");
}

void Call::respond(Response&& response)
{
	server->log("Call from " + sock.host + " for path: " + path
		+ " ; Response: " + to_string(response.get_code()));

	const auto str = response.dump(http_version,
		(method != HEAD && method != DELETE && method != TRACE));

	sock_write(sock.fd, str.c_str(), str.size()); // TODO Send `\0`?

	if(response.get_code() >= 400
		|| (has_field("Connection") && get_field("Connection") == "close")) {
		sock_close(sock.fd);
	}
}
