#include "../copper.hpp"

using namespace Copper;

const unordered_map<http_code_t, string> code_names = {
	{CONTINUE, "Continue"},
	{SWITCHING_PROTOCOLS, "Switching Protocols"},
	{PROCESSING, "Processing"},
	{OK, "OK"},
	{CREATED, "Created"},
	{ACCEPTED, "Accepted"},
	{NON_AUTHORITATIVE_INFORMATION, "Non-Authoritative Information"},
	{NO_CONTENT, "No Content"},
	{RESET_CONTENT, "Reset Content"},
	{PARTIAL_CONTENT, "Partial Content"},
	{MULTI_STATUS, "Multi-Status"},
	{ALREADY_REPORTED, "Already Reported"},
	{IM_USED, "IM Used"},
	{MULTIPLE_CHOICES, "Multiple Choices"},
	{MOVED_PERMANENTELY, "Moved Permanently"},
	{FOUND, "Found"},
	{SEE_OTHER, "See Other"},
	{NOT_MODIFIED, "Not Modified"},
	{USE_PROXY, "Use Proxy"},
	{SWITCH_PROXY, "Switch Proxy"},
	{TEMPORARY_REDIRECT, "Temporary Redirect"},
	{PERMANENT_REDIRECT, "Permanent Redirect"},
	{BAD_REQUEST, "Bad Request"},
	{UNAUTHORIZED, "Unauthorized"},
	{PAYMENT_REQUIRED, "Payment Required"},
	{FORBIDDEN, "Forbidden"},
	{NOT_FOUND, "Not Found"},
	{METHOD_NOT_ALLOWED, "Method Not Allowed"},
	{NOT_ACCEPTABLE, "Not Acceptable"},
	{PROXY_AUTHENTICATION_REQUIRED, "Proxy Authentication Required"},
	{REQUEST_TIMEOUT, "Request Timeout"},
	{CONFLICT, "Conflict"},
	{GONE, "Gone"},
	{LENGTH_REQUIRED, "Length Required"},
	{PRECONDITION_FAILED, "Precondition Failed"},
	{PAYLOAD_TOO_LARGE, "Payload Too Large"},
	{URI_TOO_LONG, "URI Too Long"},
	{UNSUPPORTED_MEDIA_TYPE, "Unsupported Media Type"},
	{RANGE_NOT_SATISFIABLE, "Range Not Satisfiable"},
	{EXPECTATION_FAILED, "Expectation Failed"},
	{IM_A_TEAPOT, "I'm a teapot"},
	{MISDIRECTED_REQUEST, "Misdirected Request"},
	{UNPROCESSABLE_ENTITY, "Unprocessable Entity"},
	{LOCKED, "Locked"},
	{FAILED_DEPENDENCY, "Failed Dependency"},
	{UPGRADE_REQUIRED, "Upgrade Required"},
	{PRECONDITION_REQUIRED, "Precondition Required"},
	{TOO_MANY_REQUESTS, "Too Many Requests"},
	{REQUEST_HEADER_FIELDS_TOO_LARGE, "Request Header Fields Too Large"},
	{UNAVAILABLE_FOR_LEGAL_REASONS, "Unavailable For Legal Reasons"},
	{INTERNAL_SERVER_ERROR, "Internal Server Error"},
	{NOT_IMPLEMENTED, "Not Implemented"},
	{BAD_GATEWAY, "Bad Gateway"},
	{SERVICE_UNAVAILABLE, "Service Unavailable"},
	{GATEWAY_TIMEOUT, "Gateway Timeout"},
	{HTTP_VERSION_NOT_SUPPORTED, "HTTP Version Not Supported"},
	{VARIANT_ALSO_NEGOTIATES, "Variant Also Negotiates"},
	{INSUFFICIENT_STORAGE, "Insufficient Storage"},
	{LOOP_DETECTED, "Loop Detected"},
	{NOT_EXTENDED, "Not Extended"},
	{NETWORK_AUTHENTICATION_REQUIRED, "Network Authentication Required"}
};

const string get_name(const http_code_t code)
{
	const auto n = code_names.find(code);
	return (n != code_names.cend() ? n->second : "?");
}

const string get_date()
{
	const auto now = time(0);
	array<char, 80> buf;
	const auto ts = *localtime(&now);
	strftime(buf.data(), buf.size(), "%a, %d %b %Y %H:M:%S %Z", &ts);

	return string(buf.data());
}

Response::Response(const http_code code)
	: code{code}
{
	set_field("Server", "CopperHTTP");
	set_field("Content-Type", "text/html");
}

void Response::set_tag(const string key, const string value)
{
	const string str("$("s + key + ")");
	const auto tag_size = str.size();
	const auto value_size = value.size();

	size_t pos = 0;

	while((pos = body.find(str, pos)) != string::npos) {
		body.replace(pos, tag_size, value);
		pos += value_size;
	}
}

string Response::dump(const string& http_version, const bool body)
{
	string buffer = http_version
		+ ' ' + to_string(code)
		+ ' ' + get_name(code) + "\r\n";

	set_field("Date", get_date());
	set_field("Content-Length", to_string(this->body.size()));

	for(const auto& f : head_fields) {
		buffer += f.first + ": " + f.second + "\r\n";
	}

	buffer += "\r\n";

	if(body) buffer += this->body;
	return buffer;
}

Response Copper::redirect(const string& to)
{
	Response response(TEMPORARY_REDIRECT);
	response.set_field("Location", to);

	return response;
}
