#ifndef COMMAND_HPP
# define COMMAND_HPP

# include<string>
# include<vector>

namespace Copper
{
	using namespace std;

	class HTTPServer;

	class Command
	{
		public:
			inline Command(const string&& name, const string&& usage,
				const string&& description)
				: name{name}, usage{usage}, description{description}
			{}

			inline const string& get_name() const
			{
				return name;
			}

			inline const string& get_usage() const
			{
				return usage;
			}

			inline const string& get_description() const
			{
				return description;
			}

			virtual void perform(HTTPServer& server,
				const vector<string>& args) const = 0;

		private:
			const string name;
			const string usage;
			const string description;
	};

	class HelpCommand : public Command
	{
		public:
			inline HelpCommand()
				: Command("help", "help", "Shows some help")
			{}

			void perform(HTTPServer& server,
				const vector<string>&) const override;
	};

	class BundlesCommand : public Command
	{
		public:
			inline BundlesCommand()
				: Command("bundles", "bundles", "Lists all bundles")
			{}

			void perform(HTTPServer& server,
				const vector<string>&) const override;
	};

	class ReloadCommand : public Command
	{
		public:
			inline ReloadCommand()
				: Command("reload", "reload",
					"Clears caches and reloads bundles")
			{}

			void perform(HTTPServer& server,
				const vector<string>&) const override;
	};

	class ExitCommand : public Command
	{
		public:
			inline ExitCommand()
				: Command("exit", "exit", "Shuts the webserver down")
			{}

			void perform(HTTPServer& server,
				const vector<string>&) const override;
	};
}

#endif
