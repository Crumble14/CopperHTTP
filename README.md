# CopperHTTP

CopperHTTP is a webserver allowing to create websites with C++ backend.



## Bundles

At startup, the server loads plugins called **bundle**s. Bundles are placed inside of the ``bundles`` directory.
A bundle contains the C++ code of a website.

The bundle's binary must be a shared library compiled with, at least, the following flags: ``-std=c++17 -shared -fPIC``

The file structure of a bundle is the following:

```
test/			# The bundle's directory (placed inside of `bundles`)
	templates/	# The templates directory (optional)
	bundle		# The file that contains informations on the bundle
	bundle.so	# The bundle's binary
	resources	# The file referencing the list of all resource directories (optional)
```



The information file requires, at least, the following informations:
- **name**: The bundle's name
- **description**: A short description of the bundle
- **verson**: The version of the bundle

Here is an example of an information file:

```
name		= Test
description	= This is a test bundle
version		= 1.0
```



The format of a ``resources`` file if the following

```
test.com:path/to/resources/directory/
```

Each line takes:
- The host(s) the resources are available on
- The path to the resources directory (beginning at the bundle's directory). The path to access the resources will be the same

**Note**: Both host and path can take wildcards (``*``)



### Bundle example

Here is an example code for a bundle:

```cpp
#include<copper.hpp>

using namespace Copper;

Bundle* b;

extern "C" void enable(Bundle& bundle)
{
	b = &bundle;
	bundle.get_server().log("Enabling bundle...");

	// Here you can register handlers for example
}

extern "C" void disable()
{
	b->get_server().log("Disabling bundle...");
}
```

The ``enable`` and ``disable`` functions are required in order to load the bundle.



### Resources

Resources are files that can be directly accessed by HTTP call.
A directory that contains resources have to be registered into the ``resources`` file.

Since a directory is registered as a resources directory, every file in it can be accessed by everyone.


## Handler

A handler is a piece of code that handles HTTP calls for a specific host/path.

Here is an example of a handle's declaration:

```cpp
class TestHandler : public Handler
{
	inline TestHandler()
		: Handler("*", "/foo")
	{}

	void handle(HTTPServer&, Call& call) const
	{
		Response response;
		response.set_body("bar");

		return response;
	}
};
```

**Note**: Both host and path can take wildcards (``*``)

The above handler can be registered using the following code:

```cpp
server.register_handler<TestHandler>();
```

where ``server`` is the ``HTTPServer`` instance.

After this handler is registered, the server will respond ``bar`` to every call to the path ``/foo``.



## Templates

Templates are stored into the ``templates`` directory of the bundle.
They can be loaded like this:

```cpp
bundle.get_template("foo");
```

The above function will load and return the content of the ``foo`` template (located at: ``templates/foo``).

A template can contain tags (example: ``$(test)``), which can be replaced by a value.
Example:

```cpp
response.set_tag("foo", "bar");
```

The above code will replace every occurence of ``$(foo)`` by ``bar`` in the response body.
